﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondLessonOTUS
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Rectangle> rectangles = new List<Rectangle>()
            {
                new Rectangle(1, 1, 4, 9),
                new Rectangle(5, 5, 4, 9)
            };

            for (byte i = 0; i < rectangles.Count; i++)
            {
                Console.WriteLine($"R{i + 1}: {rectangles[i]}");
                rectangles[i].Draw();
            }

            if (rectangles[0] == rectangles[1]) Console.WriteLine("The rectangles are equal.");
            if (rectangles[0] != rectangles[1]) Console.WriteLine("The rectangles are not equal.");

            Console.WriteLine("\nSquare:");
            for (byte i = 0; i < rectangles.Count; i++)
            {
                Console.WriteLine($"R{i + 1}: {rectangles[i].GetSquare()}");
            }
            Console.WriteLine("\nPerimeter:");
            for (byte i = 0; i < rectangles.Count; i++)
            {
                Console.WriteLine($"R{i + 1}: {rectangles[i].GetPerimeter()}");
            }
            Rectangle Sum = rectangles[0] + rectangles[1];
            Console.WriteLine("\nSum: " + Sum + ". Square is " + Sum.GetSquare());
            Sum.Draw();

            Rectangle subtraction = rectangles[0] - rectangles[1];
            if (subtraction is null)
                Console.WriteLine("Subtraction action reports: Congratulations! You have killed all of the rectangles!");
            else
            {
                subtraction.Draw();
                Console.WriteLine("Subtraction: " + subtraction + ". Square is " + subtraction.GetSquare());
            }


            Console.ReadLine();
        }
    }
}
