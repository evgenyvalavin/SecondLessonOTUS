﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondLessonOTUS
{
    public class Rectangle
    {
        int x, y;
        decimal height, width;

        public Rectangle(int x, int y, decimal height, decimal width)
        {
            this.x = x;
            this.y = y;
            this.height = height;
            this.width = width;
        }
        /// <summary>
        /// First parameter may be "coordinates" and "sizes" only. Second parameter may be "1" or "2"
        /// </summary>
        /// <param name="type"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        public decimal this[string type, int i]
        {
            get
            {
                switch (type)
                {
                    case "coordinates":
                        {
                            switch (i)
                            {
                                case 1: return x;
                                case 2: return y;
                                default: throw new IndexOutOfRangeException();
                            }
                        }
                    case "sizes":
                        {
                            switch (i)
                            {
                                case 1: return height;
                                case 2: return width;
                                default: throw new IndexOutOfRangeException();
                            }
                        }
                    default: throw new IndexOutOfRangeException();
                }

            }
        }
        /// <summary>
        /// Fills the smallest rectangle into the laggest one and returns it's value
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public static Rectangle operator +(Rectangle first, Rectangle second)
        {
            Rectangle ResultRectangle = null;
            decimal s1, s2, s_new;
            s1 = first.width * first.height;
            s2 = second.width * second.height;
            s_new = Math.Floor(s1 + s2);
            if (s1 > s2)
            {
                decimal height = first.height, width = first.width;
                bool doAgain = true;
                while (doAgain)
                {
                    height += (decimal)0.01;
                    width += (decimal)0.01;
                    var non = height * width;
                    if (Math.Floor(non) == s_new)
                    {
                        doAgain = false;
                        ResultRectangle = new Rectangle(first.x, first.y, height, width);
                    }
                }
            }
            else
            {
                decimal height = second.height, width = second.width;
                bool doAgain = true;
                while (doAgain)
                {
                    height += (decimal)0.01;
                    width += (decimal)0.01;
                    var non = height * width;
                    if (Math.Floor(non) == s_new)
                    {
                        doAgain = false;
                        ResultRectangle = new Rectangle(second.x, second.y, height, width);
                    }
                }
            }
            return ResultRectangle;
        }

        /// <summary>
        /// Extracts the smallest rectangle from the laggest one and returns the value of the new rectangle it was extracted from
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public static Rectangle operator -(Rectangle first, Rectangle second)
        {
            Rectangle ResultRectangle = null;
            decimal s1, s2, s_new;
            s1 = first.width * first.height;
            s2 = second.width * second.height;
            if (s1 == s2) return null;
            if (s1 > s2)
            {
                s_new = Math.Floor(s1 - s2);
                decimal height = first.height, width = first.width;
                bool doAgain = true;
                while (doAgain)
                {
                    height -= (decimal)0.01;
                    width -= (decimal)0.01;
                    var non = height * width;
                    if (Math.Floor(non) == s_new)
                    {
                        doAgain = false;
                        ResultRectangle = new Rectangle(first.x, first.y, height, width);
                    }
                }
            }
            else
            {
                decimal height = second.height, width = second.width;
                s_new = Math.Floor(s2 - s1);
                bool doAgain = true;
                while (doAgain)
                {
                    height -= (decimal)0.01;
                    width -= (decimal)0.01;
                    var non = height * width;
                    if (Math.Floor(non) == s_new)
                    {
                        doAgain = false;
                        ResultRectangle = new Rectangle(second.x, second.y, height, width);
                    }
                }
            }
            return ResultRectangle;

        }

        /// <summary>
        /// Gets the square of the specified rectangle
        /// </summary>
        /// <returns></returns>
        public decimal GetSquare()
        {
            return Math.Floor(height * width);
        }
        /// <summary>
        /// Gets the perimeter of the specified rectangle
        /// </summary>
        /// <returns></returns>
        public decimal GetPerimeter()
        {
            return Math.Floor(height + width);
        }

        public override string ToString()
        {
            return $"x={x}, y={y}, heigh={height}, width={width}";
        }
        /// <summary>
        /// Displays the specified rectangle in the console window
        /// </summary>
        public void Draw()
        {
            string corn = "╔", space = "", temp = "";
            for (int i = 0; i < width; i++)
            {
                space += "  ";
                corn += "══";
            }
            corn += "═╗" + "\n";
            space += " ";

            for (int i = 0; i < height; i++)
                corn += temp + "║" + space + "║" + "\n";

            corn += temp + "╚═";
            for (int i = 0; i < width; i++)
                corn += "══";

            corn += "╝" + "\n";

            Console.Write(corn);
        }

        public static bool operator ==(Rectangle first, Rectangle second)
        {
            for (byte i = 1; i <= 2; i++)
            {
                if (first["sizes", i] != second["sizes", i]) return false;
            }
            return true;
        }
        public static bool operator !=(Rectangle first, Rectangle second)
        {
            for (byte i = 1; i <= 2; i++)
            {
                if (first["sizes", i] != second["sizes", i]) return true;
            }
            return false;
        }
    }
}
